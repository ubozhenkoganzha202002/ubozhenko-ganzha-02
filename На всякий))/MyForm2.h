#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;

const int arraySize = 100;

struct M {
	string Name;
	int Quantity;
	int Price;
};

namespace WindowsF {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm2
	/// </summary>
	public ref class MyForm2 : public System::Windows::Forms::Form
	{
	public:
		MyForm2(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm2()
		{
			if (components)
			{
				delete components;
			}
		}

	private: System::Windows::Forms::Label^ label1;
	private: System::Windows::Forms::DataGridView^ dataGridView1;
	private: System::Windows::Forms::Button^ button2;
	private: System::Windows::Forms::Button^ button3;
	private: System::Windows::Forms::FlowLayoutPanel^ flowLayoutPanel1;
	private: System::Windows::Forms::TextBox^ textBox2;
	private: System::Windows::Forms::Label^ label3;
	private: System::Windows::Forms::Button^ button1;
	private: System::Windows::Forms::TextBox^ textBox1;
	private: System::Windows::Forms::Label^ label2;
	private: System::Windows::Forms::Panel^ panel1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ID;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ������������;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ����������;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ����;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^ ����_������;


	protected:

	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->dataGridView1 = (gcnew System::Windows::Forms::DataGridView());
			this->ID = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->������������ = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->���������� = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->���� = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->����_������ = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->flowLayoutPanel1 = (gcnew System::Windows::Forms::FlowLayoutPanel());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->BeginInit();
			this->flowLayoutPanel1->SuspendLayout();
			this->panel1->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Dock = System::Windows::Forms::DockStyle::Left;
			this->label1->Font = (gcnew System::Drawing::Font(L"Mistral", 25.81132F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(0, 0);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(121, 46);
			this->label1->TabIndex = 2;
			this->label1->Text = L"�������";
			// 
			// dataGridView1
			// 
			this->dataGridView1->AllowUserToAddRows = false;
			this->dataGridView1->AllowUserToDeleteRows = false;
			this->dataGridView1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom)
				| System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->dataGridView1->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::AllCells;
			this->dataGridView1->AutoSizeRowsMode = System::Windows::Forms::DataGridViewAutoSizeRowsMode::DisplayedHeaders;
			this->dataGridView1->ColumnHeadersHeight = 25;
			this->dataGridView1->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dataGridView1->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {
				this->ID, this->������������,
					this->����������, this->����, this->����_������
			});
			this->dataGridView1->Location = System::Drawing::Point(39, 59);
			this->dataGridView1->Name = L"dataGridView1";
			this->dataGridView1->ReadOnly = true;
			this->dataGridView1->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::AutoSizeToAllHeaders;
			this->dataGridView1->Size = System::Drawing::Size(595, 249);
			this->dataGridView1->TabIndex = 3;
			this->dataGridView1->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &MyForm2::dataGridView1_CellContentClick);
			// 
			// ID
			// 
			this->ID->Frozen = true;
			this->ID->HeaderText = L"ID";
			this->ID->MinimumWidth = 6;
			this->ID->Name = L"ID";
			this->ID->ReadOnly = true;
			this->ID->Width = 46;
			// 
			// ������������
			// 
			this->������������->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->������������->HeaderText = L"������������";
			this->������������->MinimumWidth = 6;
			this->������������->Name = L"������������";
			this->������������->ReadOnly = true;
			// 
			// ����������
			// 
			this->����������->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->����������->HeaderText = L"����������";
			this->����������->MinimumWidth = 6;
			this->����������->Name = L"����������";
			this->����������->ReadOnly = true;
			// 
			// ����
			// 
			this->����->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->����->HeaderText = L"���� 1 ������� ������";
			this->����->MinimumWidth = 6;
			this->����->Name = L"����";
			this->����->ReadOnly = true;
			// 
			// ����_������
			// 
			this->����_������->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->����_������->HeaderText = L"���� ������";
			this->����_������->MinimumWidth = 6;
			this->����_������->Name = L"����_������";
			this->����_������->ReadOnly = true;
			// 
			// button2
			// 
			this->button2->BackColor = System::Drawing::Color::BurlyWood;
			this->button2->Font = (gcnew System::Drawing::Font(L"Mistral", 16.30189F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button2->Location = System::Drawing::Point(325, 3);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(104, 38);
			this->button2->TabIndex = 14;
			this->button2->Text = L"������";
			this->button2->UseVisualStyleBackColor = false;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm2::button2_Click);
			// 
			// button3
			// 
			this->button3->BackColor = System::Drawing::Color::BurlyWood;
			this->button3->Font = (gcnew System::Drawing::Font(L"Mistral", 16.30189F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button3->Location = System::Drawing::Point(435, 3);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(104, 38);
			this->button3->TabIndex = 15;
			this->button3->Text = L"��������";
			this->button3->UseVisualStyleBackColor = false;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm2::button3_Click);
			// 
			// flowLayoutPanel1
			// 
			this->flowLayoutPanel1->AutoSize = true;
			this->flowLayoutPanel1->Controls->Add(this->button3);
			this->flowLayoutPanel1->Controls->Add(this->button2);
			this->flowLayoutPanel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->flowLayoutPanel1->FlowDirection = System::Windows::Forms::FlowDirection::RightToLeft;
			this->flowLayoutPanel1->Location = System::Drawing::Point(121, 0);
			this->flowLayoutPanel1->Name = L"flowLayoutPanel1";
			this->flowLayoutPanel1->Size = System::Drawing::Size(542, 44);
			this->flowLayoutPanel1->TabIndex = 16;
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(0, 92);
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(443, 20);
			this->textBox2->TabIndex = 10;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Mistral", 18.33962F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label3->Location = System::Drawing::Point(16, 59);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(117, 33);
			this->label3->TabIndex = 12;
			this->label3->Text = L"����������";
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->BackColor = System::Drawing::Color::BurlyWood;
			this->button1->Font = (gcnew System::Drawing::Font(L"Mistral", 16.30189F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->Location = System::Drawing::Point(482, 346);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(144, 75);
			this->button1->TabIndex = 13;
			this->button1->Text = L"������� �� �������";
			this->button1->UseVisualStyleBackColor = false;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm2::button1_Click);
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(0, 36);
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(443, 20);
			this->textBox1->TabIndex = 9;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Mistral", 18.33962F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(16, 0);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(140, 33);
			this->label2->TabIndex = 11;
			this->label2->Text = L"������������";
			// 
			// panel1
			// 
			this->panel1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left)
				| System::Windows::Forms::AnchorStyles::Right));
			this->panel1->AutoSize = true;
			this->panel1->Controls->Add(this->label2);
			this->panel1->Controls->Add(this->textBox1);
			this->panel1->Controls->Add(this->label3);
			this->panel1->Controls->Add(this->textBox2);
			this->panel1->Location = System::Drawing::Point(12, 314);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(464, 115);
			this->panel1->TabIndex = 17;
			// 
			// MyForm2
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(106, 106);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoSizeMode = System::Windows::Forms::AutoSizeMode::GrowAndShrink;
			this->BackColor = System::Drawing::Color::SandyBrown;
			this->ClientSize = System::Drawing::Size(663, 441);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->flowLayoutPanel1);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->dataGridView1);
			this->Controls->Add(this->label1);
			this->Name = L"MyForm2";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"�������";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridView1))->EndInit();
			this->flowLayoutPanel1->ResumeLayout(false);
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();
			//
			M catalog[arraySize];
			ifstream crate;
			crate.open("crate.txt");
			char buff[15];
			int i = 0;
			while (i < 100)
			{
				this->dataGridView1->Rows->Add();
				crate.getline(buff, 15);
				catalog[i].Name = buff;
				crate.getline(buff, 15);
				catalog[i].Quantity = atoi(buff);
				crate.getline(buff, 15);
				catalog[i].Price = atoi(buff);
				i++;
			}
			crate.close();
			i = 0;
			String^ fakieString = "";
			while (catalog[i].Name != "") {
				int Number = 0;
				this->dataGridView1->Rows[i]->Cells[Number]->Value = "�" + (i + 1) + ".";
				Number++;
				fakieString = gcnew String(catalog[i].Name.c_str());
				this->dataGridView1->Rows[i]->Cells[Number]->Value = fakieString;
				Number++;
				this->dataGridView1->Rows[i]->Cells[Number]->Value = Convert::ToString(catalog[i].Quantity);
				Number++;
				this->dataGridView1->Rows[i]->Cells[Number]->Value = catalog[i].Price;
				Number++;
				this->dataGridView1->Rows[i]->Cells[Number]->Value = catalog[i].Price * catalog[i].Quantity;
				i++;
			}

		}
#pragma endregion
	private: System::Void dataGridView1_CellContentClick(System::Object^ sender, System::Windows::Forms::DataGridViewCellEventArgs^ e) {
	}
private: System::Void button3_Click(System::Object^ sender, System::EventArgs^ e) {
	M catalog[arraySize];
	ifstream crate;
	crate.open("crate.txt");
	char buff[15];
	int i = 0;
	while (i < 100)
	{
		this->dataGridView1->Rows->Add();
		crate.getline(buff, 15);
		catalog[i].Name = buff;
		crate.getline(buff, 15);
		catalog[i].Quantity = atoi(buff);
		crate.getline(buff, 15);
		catalog[i].Price = atoi(buff);
		i++;
	}
	crate.close();
	i = 0;
	String^ fakieString = "";
	while (catalog[i].Name != "") {
		int Number = 0;
		this->dataGridView1->Rows[i]->Cells[Number]->Value = "�" + (i + 1) + ".";
		Number++;
		fakieString = gcnew String(catalog[i].Name.c_str());
		this->dataGridView1->Rows[i]->Cells[Number]->Value = fakieString;
		Number++;
		this->dataGridView1->Rows[i]->Cells[Number]->Value = Convert::ToString(catalog[i].Quantity);
		Number++;
		this->dataGridView1->Rows[i]->Cells[Number]->Value = catalog[i].Price;
		Number++;
		this->dataGridView1->Rows[i]->Cells[Number]->Value = catalog[i].Price * catalog[i].Quantity;
		i++;
	}
}
private: System::Void button1_Click(System::Object^ sender, System::EventArgs^ e) {
	M catalog[arraySize];
	ifstream crate;
	crate.open("crate.txt");
	char buff[15];
	int i = 0;
	while (i < 100)
	{
		this->dataGridView1->Rows->Add();
		crate.getline(buff, 15);
		catalog[i].Name = buff;
		crate.getline(buff, 15);
		catalog[i].Quantity = atoi(buff);
		crate.getline(buff, 15);
		catalog[i].Price = atoi(buff);
		i++;
	}
	crate.close();
	ofstream deleteCrate("crate.txt", ios::trunc);
	deleteCrate << "";
	i = 0;
	while (catalog[i].Name != "") {
		String^ fakieString = gcnew String(catalog[i].Name.c_str());
		if ((textBox1->Text == fakieString) && (Convert::ToDouble(textBox2->Text) < catalog[i].Quantity)) {
			deleteCrate << catalog[i].Name << endl;
			deleteCrate << catalog[i].Quantity - Convert::ToDouble(textBox2->Text) << endl;
			deleteCrate << catalog[i].Price << endl;
		}
		else if (textBox1->Text == fakieString) {
			deleteCrate << catalog[i].Name << endl;
			deleteCrate << catalog[i].Quantity << endl;
			deleteCrate << catalog[i].Price << endl;
		}
		i++;
	}
	deleteCrate.close();
	textBox1->Text = "";
	textBox2->Text = "";
}
private: System::Void button2_Click(System::Object^ sender, System::EventArgs^ e) { // �������
	M catalog[arraySize], crateBuy[arraySize];
	ifstream fin; // ���������� ������ �� ������
	fin.open("file.txt");
	char buff[15];
	int i = 0;
	while (i < 100)
	{
		fin.getline(buff, 15);
		catalog[i].Name = buff;
		fin.getline(buff, 15);
		catalog[i].Quantity = atoi(buff);
		fin.getline(buff, 15);
		catalog[i].Price = atoi(buff);
		i++;
	}
	fin.close();
	i = 0;
	ifstream crate;
	crate.open("crate.txt");
	while (i < 100) {
		crate.getline(buff, 15);
		crateBuy[i].Name = buff;
		crate.getline(buff, 15);
		crateBuy[i].Quantity = atoi(buff);
		crate.getline(buff, 15);
		crateBuy[i].Price = atoi(buff);
		i++;
	}
	crate.close();
	i = 0;
	while (catalog[i].Name != "") {
		int j = 0;
		while (crateBuy[j].Name != "") {
			if (crateBuy[j].Name == catalog[i].Name) {
				catalog[i].Quantity = Convert::ToDouble(catalog[i].Quantity) - Convert::ToDouble(crateBuy[j].Quantity);
			}
			j++;
		}
		i++;
	}
	i = 0;
	ofstream deleteCatalog ("file.txt", ios::trunc); // ������� ����� ��������
	deleteCatalog << "";
	deleteCatalog.close();
	ofstream fout("file.txt"); // ������ � ������� ���������� ������
	while (catalog[i].Name != "") {
		if (catalog[i].Quantity > 0) {
			fout << catalog[i].Name << endl;
			fout << catalog[i].Quantity << endl;
			fout << catalog[i].Price << endl;
		}
		i++;
	}
	fout.close();
	ofstream crateDelete("crate.txt", ios::trunc); // ������� �������
	crateDelete << "";
	crateDelete.close();
	crate.open("crate.txt"); // ����� ������ �����
	i = 0;
	while (i < 100)
	{
		dataGridView1->Rows->Clear();
		crate.getline(buff, 15);
		catalog[i].Name = buff;
		crate.getline(buff, 15);
		catalog[i].Quantity = atoi(buff);
		crate.getline(buff, 15);
		catalog[i].Price = atoi(buff);
		i++;
	}
	crate.close();
}
};
}
